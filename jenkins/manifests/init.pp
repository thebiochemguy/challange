#Module to install jenkins
class jenkins {

$jenkins_port = 8000
$os_ver = Numeric($::os[release][major])

#Code design around the idea that service and iptables are being deprecited and distros are moving to systemd and firewalld/ufw
if $::os[name] == 'CentOS' and $os_ver > 6 {
        exec {'jenkinsFirewall':
          command => "firewall-cmd --zone=public --add-port='${jenkins_port}'/tcp --permanent",
          path    => '/bin/',
          onlyif  => "/bin/test `/bin/firewall-cmd --list-ports | grep -c '${jenkins_port}'` -eq 0 -a `systemctl is-active firewalld.service` == 'active'"
        }
        service {'firewalld':
          ensure    => running,
          enable    => true,
          subscribe => Exec['jenkinsFirewall'],
        }

        file {'/etc/yum.repos.d/jenkins.repo':
          ensure => 'file',
          source => 'https://pkg.jenkins.io/redhat/jenkins.repo',
          mode   => '0644'
        }

        exec {'import jenkins gpg key':
          command     => '/bin/rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key',
          subscribe   => File['/etc/yum.repos.d/jenkins.repo'],
          refreshonly => true
        }

        $jenkins_pak = ['java-11-openjdk','jenkins']
        package { $jenkins_pak: }

        exec {'Change Jenkins port':
          command => "/bin/sed -i 's/Environment=\"JENKINS_PORT=.*/Environment=\"JENKINS_PORT=${jenkins_port}\"/g' /lib/systemd/system/jenkins.service",
          onlyif  => "/bin/test `grep -c Environment=\\\"JENKINS_PORT=${jenkins_port}\\\" /lib/systemd/system/jenkins.service` -eq 0"
        }

        service { 'jenkins':
          ensure    => running,
          enable    => true,
          subscribe => Exec['Change Jenkins port']
        }


}elsif $::os[name] == 'Ubuntu' and $os_ver > 15{

        #Only update the firewall if it doese not have the open port and is active.
        exec {'jenkinsFirewall':
          command => "ufw allow '${jenkins_port}'/tcp",
          path    => '/usr/sbin/',
          onlyif  => "/bin/test `/usr/sbin/ufw status | /usr/bin/grep -c '${jenkins_port}'` -eq 0 -a `ufw status | /usr/bin/grep -c Status:\ active` -gt 0"
        }
        service {'ufw':
          ensure    => running,
          enable    => true,
          subscribe => Exec['jenkinsFirewall']
        }

        file {'jenkins_repo':
          ensure  => 'file',
          path    => '/etc/apt/sources.list.d/jenkins.list',
          content => 'deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/',
          mode    => '0644'
        }
        file {'jenkins_keys':
          ensure => 'file',
          path   => '/usr/share/keyrings/jenkins-keyring.asc',
          source => 'https://pkg.jenkins.io/debian-stable/jenkins.io.key'
        }
        exec {'import jenkins key':
          command     => '/usr/bin/apt-get update',
          subscribe   => File['jenkins_repo'],
          refreshonly => true
        }

        $jenkins_pak = ['fontconfig','openjdk-11-jre','jenkins']
        package { $jenkins_pak: }

        exec {'Change Jenkins port':
          command => "/bin/sed -i 's/Environment=\"JENKINS_PORT=.*/Environment=\"JENKINS_PORT=${jenkins_port}\"/g' /lib/systemd/system/jenkins.service",
          onlyif  => "/bin/test `grep -c Environment=\\\"JENKINS_PORT=${jenkins_port}\\\" /lib/systemd/system/jenkins.service` -eq 0"
        }

        service { 'jenkins':
          ensure    => running,
          enable    => true,
          subscribe => Exec['Change Jenkins port']
        }
}

}
